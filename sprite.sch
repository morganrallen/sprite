EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5250 2750 5350 2750
Wire Wire Line
	5250 2650 5350 2650
$Comp
L LED:APA102-2020 D5
U 1 1 5CE6EFA5
P 4950 2750
F 0 "D5" H 4950 3231 50  0000 C CNN
F 1 "APA102-2020" H 4950 3140 50  0000 C CNN
F 2 "sprite:LED-APA102-2020" H 5000 2450 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201604/APA102-2020%20SMD%20LED.pdf" H 5050 2375 50  0001 L TNN
	1    4950 2750
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102-2020 D6
U 1 1 5CE6EB53
P 5650 2750
F 0 "D6" H 5650 3231 50  0000 C CNN
F 1 "APA102-2020" H 5650 3140 50  0000 C CNN
F 2 "sprite:LED-APA102-2020" H 5700 2450 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201604/APA102-2020%20SMD%20LED.pdf" H 5750 2375 50  0001 L TNN
	1    5650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2750 4650 2750
Wire Wire Line
	4550 2650 4650 2650
Wire Wire Line
	3850 2750 3950 2750
Wire Wire Line
	3850 2650 3950 2650
$Comp
L LED:APA102-2020 D3
U 1 1 5CE719F5
P 3550 2750
F 0 "D3" H 3550 3231 50  0000 C CNN
F 1 "APA102-2020" H 3550 3140 50  0000 C CNN
F 2 "sprite:LED-APA102-2020" H 3600 2450 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201604/APA102-2020%20SMD%20LED.pdf" H 3650 2375 50  0001 L TNN
	1    3550 2750
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102-2020 D4
U 1 1 5CE719FF
P 4250 2750
F 0 "D4" H 4250 3231 50  0000 C CNN
F 1 "APA102-2020" H 4250 3140 50  0000 C CNN
F 2 "sprite:LED-APA102-2020" H 4300 2450 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201604/APA102-2020%20SMD%20LED.pdf" H 4350 2375 50  0001 L TNN
	1    4250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2750 3250 2750
Wire Wire Line
	3150 2650 3250 2650
Wire Wire Line
	2450 2750 2550 2750
Wire Wire Line
	2450 2650 2550 2650
$Comp
L LED:APA102-2020 D1
U 1 1 5CE73BF7
P 2150 2750
F 0 "D1" H 2150 3231 50  0000 C CNN
F 1 "APA102-2020" H 2150 3140 50  0000 C CNN
F 2 "sprite:LED-APA102-2020" H 2200 2450 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201604/APA102-2020%20SMD%20LED.pdf" H 2250 2375 50  0001 L TNN
	1    2150 2750
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102-2020 D2
U 1 1 5CE73C01
P 2850 2750
F 0 "D2" H 2850 3231 50  0000 C CNN
F 1 "APA102-2020" H 2850 3140 50  0000 C CNN
F 2 "sprite:LED-APA102-2020" H 2900 2450 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201604/APA102-2020%20SMD%20LED.pdf" H 2950 2375 50  0001 L TNN
	1    2850 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5CE75DD7
P 2150 2100
F 0 "TP3" H 2208 2218 50  0000 L CNN
F 1 "TestPoint" H 2208 2127 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2350 2100 50  0001 C CNN
F 3 "~" H 2350 2100 50  0001 C CNN
	1    2150 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5CE7691D
P 2150 3350
F 0 "TP4" H 2092 3376 50  0000 R CNN
F 1 "TestPoint" H 2092 3467 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2350 3350 50  0001 C CNN
F 3 "~" H 2350 3350 50  0001 C CNN
	1    2150 3350
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5CE76FDC
P 1650 2650
F 0 "TP1" V 1845 2722 50  0000 C CNN
F 1 "TestPoint" V 1750 2700 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 1850 2650 50  0001 C CNN
F 3 "~" H 1850 2650 50  0001 C CNN
	1    1650 2650
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5CE77303
P 1650 2750
F 0 "TP2" V 1450 2750 50  0000 C CNN
F 1 "TestPoint" V 1550 2800 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 1850 2750 50  0001 C CNN
F 3 "~" H 1850 2750 50  0001 C CNN
	1    1650 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 2650 1850 2650
Wire Wire Line
	1650 2750 1850 2750
Wire Wire Line
	2150 2450 2150 2400
Wire Wire Line
	2150 3050 2150 3150
Wire Wire Line
	2150 3150 2850 3150
Wire Wire Line
	2850 3150 2850 3050
Connection ~ 2150 3150
Wire Wire Line
	2150 3150 2150 3350
Wire Wire Line
	2850 3150 3550 3150
Wire Wire Line
	3550 3150 3550 3050
Connection ~ 2850 3150
Wire Wire Line
	3550 3150 4250 3150
Wire Wire Line
	4250 3150 4250 3050
Connection ~ 3550 3150
Wire Wire Line
	4250 3150 4950 3150
Wire Wire Line
	4950 3150 4950 3050
Connection ~ 4250 3150
Wire Wire Line
	4950 3150 5650 3150
Wire Wire Line
	5650 3150 5650 3050
Connection ~ 4950 3150
Wire Wire Line
	2150 2400 2850 2400
Wire Wire Line
	2850 2400 2850 2450
Connection ~ 2150 2400
Wire Wire Line
	2150 2400 2150 2100
Wire Wire Line
	2850 2400 3550 2400
Wire Wire Line
	3550 2400 3550 2450
Connection ~ 2850 2400
Wire Wire Line
	3550 2400 4250 2400
Wire Wire Line
	4250 2400 4250 2450
Connection ~ 3550 2400
Wire Wire Line
	4250 2400 4950 2400
Wire Wire Line
	4950 2400 4950 2450
Connection ~ 4250 2400
Wire Wire Line
	4950 2400 5650 2400
Wire Wire Line
	5650 2400 5650 2450
Connection ~ 4950 2400
Text Label 1650 2650 0    50   ~ 0
SDI
Text Label 1650 2750 0    50   ~ 0
CKI
Text Label 2150 3150 0    50   ~ 0
GND
Text Label 2150 2200 0    50   ~ 0
VCC
$EndSCHEMATC
